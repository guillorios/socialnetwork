@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Comentarios</div>

                    <h1>Crear Comentario</h1></h1>

                    <form action="{{ route('comments.store') }}" method="post">
                        @csrf
                        <label for="">Comentario</label>
                        <textarea name="description" id="" cols="30" rows="10"></textarea>
                        <label for="">Likes</label>
                        <input type="text" name="likes">
                        <label for="">Dislikes</label>
                        <input type="text" name="dislikes">
                        <button type="submit">Guardar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
