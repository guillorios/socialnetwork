@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Comentario</div>

                <h1>Mostra Comentario</h1></h1>

                <p>Descripcion :</p> {{ $comment->description }}</p>
                <p>Likes :</p> {{ $comment->likes }}</p>
                <p>Dislikes :</p> {{ $comment->dislikes }}</p>

            </div>

            <a href="{{ route('comments.index') }}">Volver a Comentarios</a>
        </div>
    </div>
</div>

@endsection
