@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Comentarios</div>

                    <h1>Crear Comentario</h1></h1>

                    <form action="{{ route('comments.update', $comment->id) }}" method="post">
                        @csrf
                        @method('PUT')

                        <label for="">Comentario</label>
                        <textarea name="description" id="" cols="30" rows="10">{{ $comment->description }}</textarea>
                        <label for="">Likes</label>
                        <input type="text" name="likes" value= {{ $comment->likes }}>
                        <label for="">Dislikes</label>
                        <input type="text" name="dislikes" value= {{ $comment->dislikes }}>
                        <button type="submit">Guardar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
