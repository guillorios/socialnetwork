@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="pull-left">
                        <div class="card-header">Comentarios</div>
                    </div>
                    <div class="pull-left p-2">
                        <a href="{{ route('comments.create') }}" class="btn btn-success"> Crear Nuevo Comentario</a>

                    </div>
                    <div class="p-3">
                        <h1>Comentarios</h1>
                        <table class="table table-bordered">
                            <thead>
                                <tr></tr>
                                    <th>ID</th>
                                    <th>Comentario</th>
                                    <th>Likes</th>
                                    <th>Dislikes</th>
                                    <th>Fecha</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    @foreach($comments as $comment)
                                        <tr>
                                            <td>{{ $comment->id }}</td>
                                            <td>{{ $comment->description }}</td>
                                            <td>{{ $comment->likes }}</td>
                                            <td>{{ $comment->dislikes }}</td>
                                            <td>{{ $comment->created_at }}</td>
                                            <td>
                                                <form action="{{ route('comments.destroy', $comment->id) }}" method="post">

                                                    <a class = "btn btn-info" href="{{ route('comments.show', $comment->id) }}">Mostrar</a>
                                                    <a class = "btn btn-primary" href="{{ route('comments.edit', $comment->id) }}">Editar</a>

                                                    @csrf
                                                    @method('DELETE')
                                                    <button type='submit' class="btn btn-danger">Eliminar</button>
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
