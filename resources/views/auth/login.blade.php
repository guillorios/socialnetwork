@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                @include('partials.validation-errors')
                <div class="card bg-light px-4 py-2">
                    <form action="{{ route('login') }}" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="row mb-3">
                                <label>Email:</label>
                                <div class="col-md-12">
                                    <input class="form-control"
                                        type="email"
                                        name="email"
                                        placeholder="Tu email..."
                                        value="{{ old('email') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label>Contraseña:</label>
                                <div class="col-md-12">
                                    <input class="form-control" type="password" name="password" placeholder="Tu contraseña...">
                                </div>
                            </div>
                            <button class="btn btn-primary btn-block" dusk="login-btn">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
