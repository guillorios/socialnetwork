@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                @include('partials.validation-errors')
                <div class="card bg-light px-4 py-2">
                    <form action="{{ route('register') }}" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="row mb-3">
                                <label>Username:</label>
                                <div class="col-md-12">
                                    <input class="form-control" type="text" name="name" placeholder="Tu nombre de usuario..." value="{{ old('name') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label>Nombre:</label>
                                <div class="col-md-12">
                                    <input class="form-control" type="text" name="first_name" placeholder="Tu nombre..."value="{{ old('first_name') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label>Apellido:</label>
                                <div class="col-md-12">
                                    <input class="form-control" type="text" name="last_name" placeholder="Tu apellido..." value="{{ old('last_name') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label>Email:</label>
                                <div class="col-md-12">
                                    <input class="form-control" type="email" name="email" placeholder="Tu email..." value="{{ old('email') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label>Contraseña:</label>
                                <div class="col-md-12">
                                    <input class="form-control" type="password" name="password" placeholder="Tu contraseña...">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label>Repite la contraseña:</label>
                                <div class="col-md-12">
                                    <input class="form-control" type="password" name="password_confirmation" placeholder="Repite tu contraseña...">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label>Avatar</label>
                                <div class="col-md-12">
                                    <input type="file" name="avatar" id="avatar" />
                                </div>
                            </div>
                            <button class="btn btn-primary btn-block" dusk="register-btn">Registro</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        FilePond.registerPlugin(
            FilePondPluginImagePreview,
        );

        // Get a reference to the file input element
        const inputElement = document.querySelector('input[id="avatar"]');

        // Create a FilePond instance
        const pond = FilePond.create(inputElement,
            {
                labelIdle: `Arrastre los archivos aquí o <span class="filepond--label-action">Explore</span>`,
            }
        );

        FilePond.setOptions({
            server: {
                url: '/upload',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                }
            }
        });

    </script>
@endsection
