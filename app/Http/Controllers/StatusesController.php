<?php

namespace App\Http\Controllers;

use App\Models\Status;
use Illuminate\Http\Request;
use App\Events\StatusCreated;
use App\Http\Resources\StatusResource;

class StatusesController extends Controller
{
    public function index(){

        return StatusResource::collection(
            Status::latest()->paginate()
        );

    }

    public function show(Status $status){

        return view('statuses.show', [
            'status' => StatusResource::make($status)
        ]);

    }

    public function store(Request $request){

        request()->validate([
            'body' => 'required|min:5'
        ]);

        $status = Status::create([
            'body' => request('body'),
            'user_id' => auth()->id()
        ]);



        /* Guardamos en la base de datos y en el storage de laravel */

        if($request->has('files')){

            foreach($request->file('files') as $file){

                $extension = $file->getClientOriginalExtension();
                $filename = uniqid().'-'.now()->timestamp;

                $custom_filename = $filename.'.'.$extension;

                $status->addMedia($file)
                    ->usingFileName($custom_filename)
                    ->toMediaCollection('media');

            }
        }

        $statusResource = StatusResource::make($status);

        StatusCreated::dispatch($statusResource);

        return  $statusResource;

    }
}
