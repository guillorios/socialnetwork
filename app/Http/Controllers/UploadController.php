<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TemporalyFile;

class UploadController extends Controller
{
    public function store(Request $request)
    {
        if($request->hasFile('avatar')) {

            $file = $request->file('avatar');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();

            $folder = uniqid() . '_' . now()->timestamp;
            $file->storeAs('avatars/tmp/' . $folder, $filename);

            TemporalyFile::create([
                'folder' => $folder,
                'filename' => $filename,
                'extension' => $extension,
            ]);

            return $folder;


        }

        return '';

    }
}
