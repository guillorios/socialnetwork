<?php

namespace App\Models;

use App\Models\Friendship;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, Notifiable, InteractsWithMedia ;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'birthday',
        'email',
        'password',
    ];

    protected $appends = [
        'avatar',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //Manejo de multimedia para los usuarios - Avatars

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatars')
              ->useDisk('avatars');
    }

    //Relaciones con otros modelos

    public function getAvatarAttribute()
    {
        return $this->avatar();
    }

    public function statuses(){
        return $this->hasMany(Status::class);
    }

    public function getRouteKeyName(){
        return 'name';
    }

    public function link(){
        return route('users.show', $this);
    }

    public function avatar(){
        return $this->getFirstMedia('avatars')->getFullUrl();
    }

    public function sendFriendRequestTo($recipient)
    {
        return $this->friendshipsRequestsSent()->firstOrCreate([
            'recipient_id' => $recipient->id,
        ]);
    }

    public function acceptFriendRequestFrom($sender)
    {
        $friendship = $this->friendshipsRequestsReceived()
            ->where(['sender_id' => $sender->id])
            ->first();

        $friendship->update([
            'status' => 'accepted'
        ]);

        return $friendship;

    }

    public function denyFriendRequestFrom($sender)
    {
        $friendship = $this->friendshipsRequestsReceived()
            ->where(['sender_id' => $sender->id])
            ->first();

        $ $friendship->update([
            'status' => 'denied'
        ]);

        return $friendship;
    }

    public function friendshipsRequestsReceived()
    {
        return $this->hasMany(Friendship::class, 'recipient_id');
    }

    public function friendshipsRequestsSent()
    {
        return $this->hasMany(Friendship::class, 'sender_id');
    }

    public function friends()
    {
        $senderFriends = $this->belongsToMany(User::class, 'friendships', 'sender_id', 'recipient_id')
            ->wherePivot('status', 'accepted')
            ->get();

        $recipientFriends = $this->belongsToMany(User::class, 'friendships', 'recipient_id', 'sender_id')
            ->wherePivot('status', 'accepted')
            ->get();

        return $senderFriends->merge($recipientFriends);

    }

}
