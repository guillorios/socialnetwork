<?php

namespace App\Models;

use App\Models\User;
use App\Models\Comment;
use App\Traits\HasLikes;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Status extends Model implements HasMedia
{
    use HasFactory, HasLikes, InteractsWithMedia;

    protected $fillable = [
        'user_id',
        'body',
    ];

    /* Creamos la colecccion de la galeria */

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('media')
            ->useDisk('media');
    }

    //Construimos las relaciones

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function images(){
        return $this->getMedia('media');
    }

    public function path(){
        return route('statuses.show', $this);
    }



}
