<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\FriendsController;
use App\Http\Controllers\StatusesController;
use App\Http\Controllers\FriendshipsController;
use App\Http\Controllers\StatusLikesController;
use App\Http\Controllers\UsersStatusController;
use App\Http\Controllers\CommentLikesController;
use App\Http\Controllers\NotificationsController;
use App\Http\Controllers\StatusCommentsController;
use App\Http\Controllers\AcceptFriendshipsController;
use App\Http\Controllers\ReadNotificationsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/home', [HomeController::class, 'index'])->name('home'); */

Route::get('/', function () {
    return view('welcome');
});

/* Statuses */

Route::get('statuses', [StatusesController::class, 'index'])->name('statuses.index');
Route::get('statuses/{status}', [StatusesController::class, 'show'])->name('statuses.show');
Route::post('statuses', [StatusesController::class, 'store'])->name('statuses.store')->middleware('auth');

/* Likes */

Route::post('statuses/{status}/likes', [StatusLikesController::class, 'store'])->name('statuses.likes.store')->middleware('auth');
Route::delete('statuses/{status}/likes', [StatusLikesController::class, 'destroy'])->name('statuses.likes.destroy')->middleware('auth');

/* Comments */

Route::post('statuses/{status}/comments', [StatusCommentsController::class, 'store'])->name('statuses.comments.store')->middleware('auth');

/* Likes Comment */

Route::post('comments/{comment}/likes', [CommentLikesController::class, 'store'])->name('comments.likes.store')->middleware('auth');
Route::delete('comments/{comment}/likes', [CommentLikesController::class, 'destroy'])->name('comments.likes.destroy')->middleware('auth');

/* Users */

Route::get('@{user}', [UsersController::class, 'show'])->name('users.show');

/* Statuses Users */

Route::get('users/{user}/statuses', [UsersStatusController::class, 'index'])->name('users.statuses.index');

/* Friends */

Route::get('friends', [FriendsController::class, 'index'])->name('friends.index')->middleware('auth');

/* Friendships */
Route::get('friendships/{recipient}', [FriendshipsController::class, 'show'])->name('friendships.show')->middleware('auth');
Route::post('friendships/{recipient}', [FriendshipsController::class, 'store'])->name('friendships.store')->middleware('auth');
Route::delete('friendships/{user}', [FriendshipsController::class, 'destroy'])->name('friendship.destroy')->middleware('auth');

/* Accept Friendships */

Route::get('friends/requests', [AcceptFriendshipsController::class, 'index'])->name('accept-friendships.index')->middleware('auth');
Route::post('accept-friendships/{sender}', [AcceptFriendshipsController::class, 'store'])->name('accept-friendships.store')->middleware('auth');
Route::delete('accept-friendships/{sender}', [AcceptFriendshipsController::class, 'destroy'])->name('accept-friendships.destroy')->middleware('auth');

/* Notifications */

Route::get('notifications', [NotificationsController::class, 'index'])->name('notifications.index')->middleware('auth');

/* Read Notifications */

Route::post('read-notifications/{notification}', [ReadNotificationsController::class, 'store'])->name('read-notifications.store')->middleware('auth');
Route::delete('read-notifications/{notification}', [ReadNotificationsController::class, 'destroy'])->name('read-notifications.destroy')->middleware('auth');

/* Uploads */

Route::post('upload', [UploadController::class, 'store']);


/* Route::resource('comments', CommentController::class); */

Auth::routes();
